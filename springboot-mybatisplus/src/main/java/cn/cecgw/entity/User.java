package cn.cecgw.entity;

import com.baomidou.mybatisplus.annotation.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;


/**
 * @version 1.0
 * @description:User
 * @description: TODO
 * @author: 赵新
 * @date: 2021/5/20 11:16
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
    @TableId(type= IdType.NONE)
    private Long id;
    private String name;
    private Integer age;
    private String email;
    @TableField(fill= FieldFill.INSERT)
    private LocalDateTime createTime;
    @TableField(fill= FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;
    @Version
    private Integer version;
    @TableLogic//逻辑删除
    private Integer deleted;
}
