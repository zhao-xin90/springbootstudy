package cn.cecgw.config;

import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.OptimisticLockerInnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import lombok.extern.log4j.Log4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @version 1.0
 * @description:MyMybatisPlusConfig
 * @description: 自定义MybatisPlus配置文件
 * @author: 赵新
 * @date: 2021/5/20 14:56
 */

@Log4j
@MapperScan(basePackages = "cn.cecgw.mapper")
@EnableTransactionManagement
@Configuration
public class MyMybatisPlusConfig {
    /**
     * 新版
     */
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        log.info("乐观锁");
        MybatisPlusInterceptor mybatisPlusInterceptor = new MybatisPlusInterceptor();
        mybatisPlusInterceptor.addInnerInterceptor(new OptimisticLockerInnerInterceptor());
        mybatisPlusInterceptor.addInnerInterceptor(new PaginationInnerInterceptor());
        return mybatisPlusInterceptor;
    }


    //注册乐观锁插件
   /* @Bean
    public OptimisticLockerInnerInterceptor optimisticLockerInnerInterceptor(){
        log.info("乐观锁2222");
        return new OptimisticLockerInnerInterceptor();
    }*/
}
