package cn.cecgw.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import lombok.extern.log4j.Log4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * @version 1.0
 * @description:MyMetaObjectHandler
 * @description: TODO
 * @author: 赵新
 * @date: 2021/5/20 14:25
 */
@Component
@Log4j
public class MyMetaObjectHandler implements MetaObjectHandler {
    @Override
    public void insertFill(MetaObject metaObject) {
    log.info("start insert fill");
     /*   this.setFieldValByName("createTime", LocalDateTime.now(,metaObject);
        this.setFieldValByName("updateTime", LocalDateTime.now(,metaObject);
        */
        this.fillStrategy(metaObject,"createTime",  LocalDateTime.now());
        this.fillStrategy(metaObject,"updateTime",  LocalDateTime.now());


    }

    @Override
    public void updateFill(MetaObject metaObject) {
        log.info("start update fill");
        this.setFieldValByName("updateTime", LocalDateTime.now(),metaObject);
    }
}
