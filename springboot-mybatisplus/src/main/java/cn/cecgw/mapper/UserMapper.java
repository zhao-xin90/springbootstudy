package cn.cecgw.mapper;

import cn.cecgw.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;


/**
 * @version 1.0
 * @description:UserMapper
 * @description: TODO
 * @author: 赵新
 * @date: 2021/5/20 11:21
 */
@Repository
public interface UserMapper extends BaseMapper<User> {
}
