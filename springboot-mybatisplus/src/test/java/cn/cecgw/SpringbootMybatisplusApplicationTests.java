package cn.cecgw;


import cn.cecgw.entity.User;
import cn.cecgw.mapper.UserMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.log4j.Log4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.constraints.Past;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
@Log4j
public class SpringbootMybatisplusApplicationTests {
    @Autowired
    UserMapper userMapper;
    @Test
    public void contextLoads() {
        System.out.println("1111");
        log.info("2222");
        List<User> userList = userMapper.selectList(null);
        userList.forEach(System.out::println);

    }
    @Test
    public void insert(){
        User user = new User();
        user.setId(47L);
        user.setName("赵龙1");
        user.setAge(32);
       // user.setEmail("145035");
        Integer insert = userMapper.insert(user);
        System.out.println(insert);
    }
    @Test
    public void update(){
        User user = new User();
        user.setId(47L);
        user.setName("赵龙4747");
        user.setAge(47);
        user.setEmail("5037@qq.com");
        Integer update = userMapper.updateById(user);
        System.out.println(update);
    }
    //单条删除
    @Test
    public void delete(){
        Integer integer = userMapper.deleteById(1L);
        System.out.println(integer);
    }
    //多条删除
    @Test
    public void deleteBatch(){
        Integer integer = userMapper.deleteBatchIds(Arrays.asList(1,2));
        System.out.println(integer);
    }
    //Map条件删除
    @Test
    public void deleteMap(){
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("age",47);
        hashMap.put("id",47);

        Integer integer = userMapper.deleteByMap(hashMap);
        System.out.println(integer);
    }
    @Test
    public void testOptimistictLock(){
        User user = userMapper.selectById(47L);
        user.setName("赵龙4747");
        user.setAge(47);
        user.setEmail("5037@qq.com");
        Integer update = userMapper.updateById(user);
        System.out.println(update);
    }
    //测试乐观锁失败！多线程下
    @Test
    public void testOptimistictLock2(){

        //线程1
        User user = userMapper.selectById(5L);
        user.setName("ChanV111");
        user.setEmail("1277077741@qq.com");
        //模拟另一个线程执行了插队操作
        User user2 = userMapper.selectById(5L);
        user2.setName("ChanV222");
        user2.setEmail("1277077741@qq.com");
        userMapper.updateById(user2);

        //自旋锁多次尝试提交
        userMapper.updateById(user);    //如果没有乐观锁就会覆盖队线程的值

    }
    //批量查询
    @Test
    public void testBatchIds(){
        userMapper.selectBatchIds(Arrays.asList(1,2,3,4)).forEach(System.out::println);
    }
    @Test
    public void testSelectByMap(){
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("name","ChanV222");
        hashMap.put("email","1277077741@qq.com");
        hashMap.put("age",24);
        userMapper.selectByMap(hashMap).forEach(System.out::println);
    }
    @Test
    public void testPage(){
        Page<User> userPage = new Page<>(1, 3);
        Page<User> page = userMapper.selectPage(userPage, null);
        System.out.println(page.getTotal());
        page.getRecords().forEach(System.out::println);
    }
    @Test
    public void testWrapper(){
        //查询name不为空的用户，并且邮箱不为空的用户，年龄大于12
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.isNotNull("name").isNotNull("email").gt("age",12);
        userMapper.selectList(wrapper).forEach(System.out::println);

    }

    @Test
    public void testWrapper2(){
        //查询名字Chanv
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("name","Chanv");
        userMapper.selectList(wrapper).forEach(System.out::println);

    }

    @Test
    public void testWrapper3(){
        //查询年龄在19到30岁之间的用户
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.between("age",19,30);
        userMapper.selectList(wrapper).forEach(System.out::println);

    }
    @Test
    public void testWrapper4(){
        //
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.notLike(false,"name","b").likeRight("name","C");
        userMapper.selectList(wrapper).forEach(System.out::println);

    }

    @Test
    public void testWrapper5(){
        //id 在子查询中查出来
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.inSql("id","select id from user where id<5");
        userMapper.selectList(wrapper).forEach(System.out::println);

    }
    @Test
    public void testWrapper6(){
        //通过id进行排序
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.inSql("id","select id from user where id<5").orderByDesc("id");
        userMapper.selectList(wrapper).forEach(System.out::println);

    }

}
