package cn.cecgw;

import org.apache.catalina.LifecycleException;
import org.apache.catalina.startup.Tomcat;
import org.junit.Test;

/**
 * @author zhaoxin
 * @version 1.0.0
 * @ClassName TomcatTest.java
 * @Description TODO
 * @createTime 2021年12月16日 09:57:00
 */
public class TomcatTest {
    @Test
    public void testTomcat() throws LifecycleException {
        Tomcat tomcat = new Tomcat();
        tomcat.addWebapp("/","D://Tomcat//Samples5");
        tomcat.setPort(8888);
        tomcat.start();
        tomcat.getServer().await();;

    }
}
