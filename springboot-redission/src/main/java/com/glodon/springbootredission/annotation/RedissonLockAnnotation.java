package com.glodon.springbootredission.annotation;

import java.lang.annotation.*;

/**分布式锁注解
 * @author zhaoxin
 * @version 1.0
 * @description: 添加到方法 ，修饰以@RequestBody JSON对象接收参数的方法
 * @date 2021/11/12 15:09
 */
@Inherited
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface RedissonLockAnnotation {

}
