package com.glodon.springbootredission.controller;

import com.glodon.springbootredission.annotation.RedissonLockAnnotation;
import com.glodon.springbootredission.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * @author zhaoxin
 * @version 1.0
 * @description: TODO
 * @date 2021/11/12 15:42
 */
@RestController
@Slf4j
public class RedissonController {
    @PostMapping("/add")
    @RedissonLockAnnotation
    public String addName(User user) {
        log.info("name==" + user.getName() + ",age==" + user.getAge());
        return "ok";
    }

    @GetMapping("/test")
    // @RedissonLockAnnotation
    public String test() {

        return "ok------test";
    }

}
