package com.glodon.springbootredission.redisson;

import java.util.concurrent.TimeUnit;

/**
 * @author zhaoxin
 * @version 1.0
 * @description: 分布式锁接口
 * @date 2021/11/12 14:13
 */
public interface DistributeLocker {

    /**
     * @description: 加锁
     * @param: lockKey key
     * @author zhaoxin
     * @date: 2021/11/12 14:21
     */
    void lock(String lockKey);

    /**
     * @description:  解锁
     * @param: lockKey key
     * @author zhaoxin
     * @date: 2021/11/12 14:21
     */
    void unlock(String lockKey);
    
    /** 
     * @description: 加锁锁,设置有效期
     * @param: lockKey  key
     * @param timeout 有效时间
     * @return:  timeout 有效时间，默认时间单位在实现类传入
     * @author zhaoxin
     * @date: 2021/11/12 14:22
     */ 
    void lock(String lockKey,int timeout);
    /**
     * @description: 加锁锁,设置有效期
     * @param: lockKey  key
     * @param timeout 有效时间
     * @param timeUnit 时间单位
     * @return:  timeout 有效时间，默认时间单位在实现类传入
     * @author zhaoxin
     * @date: 2021/11/12 14:22
     */
    void lock(String lockKey, int timeout, TimeUnit timeUnit);
    
    /** 
     * @description: 尝试获取锁，获取到则持有该锁返回true,未获取到立即返回false
     * @param: lockKey key
     * @return:   true-获取锁成功 false-获取锁失败
     * @author zhaoxin
     * @date: 2021/11/12 14:20
     */ 
    boolean tryLock(String lockKey);
    
    /** 
     * @description: 尝试获取锁，获取到则持有该锁leaseTime时间.
     *   若未获取到，在waitTime时间内一直尝试获取，超过watiTime还未获取到则返回false
     * @param lockKey   key
     * @param waitTime  尝试获取时间
     * @param leaseTime 锁持有时间
     * @param unit      时间单位
     * @return:  true-获取锁成功 false-获取锁失败
     * @author zhaoxin
     * @date: 2021/11/12 14:20
     */ 
    boolean tryLocK(String lockKey,long waitTime,long leaseTime,TimeUnit unit) throws InterruptedException;

    /**
     * @description: 锁是否被任意一个线程锁持有
     * @param: lockKey
     * @return: true-被锁 false-未被锁
     * @author zhaoxin
     * @date: 2021/11/12 14:19
     */
    boolean isLock(String lockKey);
}
