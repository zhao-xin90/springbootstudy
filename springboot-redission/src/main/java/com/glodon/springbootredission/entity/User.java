package com.glodon.springbootredission.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author zhaoxin
 * @version 1.0
 * @description: 用户实体类
 * @date 2021/11/12 15:54
 */
@AllArgsConstructor
@Data
@NoArgsConstructor
public class User implements Serializable {
    private String name;
    private String age;
}
