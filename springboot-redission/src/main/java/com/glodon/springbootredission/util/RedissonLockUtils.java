package com.glodon.springbootredission.util;

import com.glodon.springbootredission.redisson.DistributeLocker;
import org.redisson.api.RLock;

import java.util.concurrent.TimeUnit;

/**
 * @author zhaoxin
 * @version 1.0
 * @description: redisson锁工具类
 * @date 2021/11/12 14:31
 */
public class RedissonLockUtils {
    private static DistributeLocker locker;

    public static void setLocker(DistributeLocker locker) {
        RedissonLockUtils.locker = locker;
    }

   
    public static void lock(String lockKey) {
       locker.lock(lockKey);

    }

   
    public static void unlock(String lockKey) {

        locker.unlock(lockKey);
    }

   
    public static void lock(String lockKey, int timeout) {
      locker.lock(lockKey,timeout);
    }

   
    public static void lock(String lockKey, int timeout, TimeUnit timeUnit) {
      locker.lock(lockKey,timeout,timeUnit);
    }

   
    public static  boolean tryLock(String lockKey) {
        return locker.tryLock(lockKey);
    }

   
    public static boolean tryLock(String lockKey, long waitTime, long leaseTime, TimeUnit unit) throws InterruptedException {
        return locker.tryLocK(lockKey,waitTime,leaseTime,unit);
    }

   
    public static boolean isLock(String lockKey) {
        return locker.isLock(lockKey);
    }
}
