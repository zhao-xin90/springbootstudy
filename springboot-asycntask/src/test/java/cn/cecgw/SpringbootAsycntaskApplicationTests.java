package cn.cecgw;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;

@SpringBootTest
class SpringbootAsycntaskApplicationTests {
    @Autowired
    JavaMailSenderImpl mailSender;
    @Test
    void contextLoads() {
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setTo("1450371249@qq.com","2957245263@qq.com","18560055671@163.com");
        simpleMailMessage.setFrom("1450371249@qq.com");
        simpleMailMessage.setSubject("测试邮件");
        simpleMailMessage.setText("这是仅仅只是一个测试邮件");
        mailSender.send(simpleMailMessage);
    }

@Test
public void contextLoads2() throws MessagingException {
    //邮件设置2：一个复杂的邮件
    MimeMessage mimeMessage = mailSender.createMimeMessage();
    MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);

    helper.setSubject("通知-明天来狂神这听课");
    helper.setText("<b style='color:red'>今天 7:30来开会</b>",true);

    //发送附件
    helper.addAttachment("1.jpg",new File("D:\\1.png"));
    helper.addAttachment("2.jpg",new File("D:\\2.png"));

    helper.setTo(new String[]{"1450371249@qq.com","2957245263@qq.com","18560055671@163.com"});
    helper.setFrom("1450371249@qq.com");


    mailSender.send(mimeMessage);
}
}