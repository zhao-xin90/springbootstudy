package cn.cecgw.controller;

import cn.cecgw.serivce.AsyncService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * @version 1.0
 * @description:AsycnController
 * @description: 异步任务
 * @author: 赵新
 * @date: 2021/5/28 9:59
 */
@RestController
@RequestMapping("/async")
public class AsyncController {

    @Autowired
    AsyncService asyncService;
    @GetMapping("/hello")
    public String hello(){
        String hello = asyncService.hello();
        Future<String> hello2 = asyncService.hello2();
        try {
            hello2.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        String hello3 = asyncService.hello3();
        System.out.println("异步方法返回了："+hello);
        return "hello";
    }

}
