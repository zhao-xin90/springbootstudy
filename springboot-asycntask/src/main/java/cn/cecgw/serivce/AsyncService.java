package cn.cecgw.serivce;

import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import java.util.concurrent.*;

/**
 * @version 1.0
 * @description:AsycnService
 * @description: 异步任务
 * @author: 赵新
 * @date: 2021/5/28 9:58
 */
@Service
public class AsyncService {
    @Async("taskExecutor")
    public String hello(){

        int b= 5/0;
    try {
    TimeUnit.SECONDS.sleep(2);
    } catch (InterruptedException e) {
        e.printStackTrace();
    }
    System.out.println(Thread.currentThread().getName()+"-----------");

        System.out.println("执行了hello方法");
        return "hello";
    }
    @Async("taskExecutor")
    public Future<String> hello2(){


        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName()+"-----------");
      // int b= 5/0;
        System.out.println("执行了hello2方法");
        return new AsyncResult<String>("hello2");
    }
    @Async("taskExecutor")
    public String hello3(){

        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName()+"-----------");

        System.out.println("执行了hello3方法");
        return "hello3";
    }
}
