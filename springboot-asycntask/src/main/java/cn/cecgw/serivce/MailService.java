package cn.cecgw.serivce;

/**
 * @version 1.0
 * @description:MailService
 * @description: TODO
 * @author: 赵新
 * @date: 2021/5/31 8:51
 */
public interface MailService {
    void sendSimpleMail(String to, String subject, String content);
    void sendHtmlMail(String to, String subject, String content);
    void sendAttachmentsMail(String to, String subject, String content, String filePath);
    void sendInlineResourceMail(String to, String subject, String content, String rscPath, String rscId);
}
