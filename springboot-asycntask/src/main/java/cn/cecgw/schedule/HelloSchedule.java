package cn.cecgw.schedule;

import org.apache.commons.logging.Log;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.annotation.Schedules;
import org.springframework.stereotype.Component;

/**
 * @version 1.0
 * @description:HelloSchedule
 * @description: TODO
 * @author: 赵新
 * @date: 2021/5/28 17:07
 */
@Component
public class HelloSchedule {
    @Scheduled(cron="0/3 * * * * ?")
    public void sayHello(){
        System.out.println("世界是如此的美丽");
    }
}
