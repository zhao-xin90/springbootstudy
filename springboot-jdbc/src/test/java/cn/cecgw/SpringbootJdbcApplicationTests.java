package cn.cecgw;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.test.context.SpringBootTest;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

@SpringBootTest
class SpringbootJdbcApplicationTests {
    @Autowired
    DataSource dataSource;
    @Autowired
    DataSource dataSource2;
    @Autowired
    DataSourceProperties dataSourceProperties;
    @Test
    void contextLoads() throws SQLException {

        System.out.println(dataSource.hashCode());
        System.out.println(dataSource2.hashCode());
        System.out.println(dataSource.getClass());
        Connection connection = dataSource.getConnection();
        System.out.println(connection);
        connection.close();
        System.out.println(dataSourceProperties.getData());
    }

}
