package cn.cecgw.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @version 1.0
 * @description:JdbcController
 * @description: TODO
 * @author: 赵新
 * @date: 2021/5/26 17:16
 */
@Api("学习JDBC")
@RestController
public class JdbcController {

    @Autowired
    JdbcTemplate jdbcTemplate;
    @ApiOperation("查询用户列表")
    @GetMapping("/list")
    public List<Map<String, Object>>  list(){
        String sql="select * from user";
        List<Map<String, Object>> list = jdbcTemplate.queryForList(sql);
        return list;
    }
    @ApiOperation("更新用户")
    @PutMapping("/update/{id}")
    public int update(@PathVariable("id")@ApiParam("用户ID") Integer id){
        String sql="update user set name = '长坝坝' where id="+id;
        int update = jdbcTemplate.update(sql);
        return update;
    }
    @ApiOperation("删除用户")
    @DeleteMapping("/delete/{id}")
    public int delete(@PathVariable("id") @ApiParam("用户ID") Integer id){
        String sql="delete from user where id="+id;
        int update = jdbcTemplate.update(sql);
        return update;
    }
    @ApiOperation("增加用户")
    @PutMapping("/add")
    public int add(){
        String sql="insert into user(name,password)values ('前十','10101010')";
        int update = jdbcTemplate.update(sql);
        return update;
    }

}
