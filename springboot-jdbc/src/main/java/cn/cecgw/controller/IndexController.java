package cn.cecgw.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @version 1.0
 * @description:IndexController
 * @description: TODO
 * @author: 赵新
 * @date: 2021/5/27 10:25
 */

@Controller
public class IndexController {
    @GetMapping({"/","/index"})
    public String index(){
        return "/index.html";
    }
}
