package cn.cecgw.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PutMapping;
import springfox.documentation.RequestHandler;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.CombinedRequestHandler;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.awt.geom.FlatteningPathIterator;
import java.util.ArrayList;

/**
 * @version 1.0
 * @description:SwaggerConfig
 * @description: TODO
 * @author: 赵新
 * @date: 2021/5/27 10:07
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket docket(Environment environment){
        Profiles profiles=Profiles.of("dev");
        final boolean flag = environment.acceptsProfiles(profiles);
        return new Docket(DocumentationType.SWAGGER_2)
                .enable(flag)
                .apiInfo(apiInfo())
                .select()
                .apis(
                        RequestHandlerSelectors
                                .basePackage("cn.cecgw.controller")
                                // .any()
                        //.none()
                        //.withClassAnnotation(Controller.class)
                      //  .withMethodAnnotation(PutMapping.class)
                )
                //.paths(PathSelectors.ant("/ad*"))
                .build().groupName("zhaoxin")
                ;
    }
    private ApiInfo apiInfo(){
        return new ApiInfo(
                "JDBC接口文档",
                "JDBC接口文档详细数据包含增删改查",
                "v1.0",
                "https://www.cec.com.cn/",
                new Contact("赵新", "http://localhost:8080/index.html", "1450371249@qq.com"),
                "Apache 2.0",
                "http://www.apache.org/licenses/LICENSE-2.0",
                new ArrayList());
    }
   /* @Bean
    public Docket docket1(){
        return new Docket(DocumentationType.SWAGGER_2).groupName("group1");
    }
    @Bean
    public Docket docket2(){
        return new Docket(DocumentationType.SWAGGER_2).groupName("group2");
    }
    @Bean
    public Docket docket3(){
        return new Docket(DocumentationType.SWAGGER_2).groupName("group3");
    }*/
}
