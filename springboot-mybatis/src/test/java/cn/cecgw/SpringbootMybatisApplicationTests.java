package cn.cecgw;

import cn.cecgw.mapper.BlogMapper;
import cn.cecgw.mapper.UserMapper;
import cn.cecgw.service.UserService;
import cn.cecgw.service.impl.UserServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SpringbootMybatisApplicationTests {
    @Autowired
    UserMapper userMapper;
    @Autowired
    BlogMapper blogMapper;
    @Test
    void contextLoads() {

        userMapper.getUserList().forEach(System.out::println);
        UserServiceImpl userService = new UserServiceImpl();
        //System.out.println(userService instanceof UserService);
        blogMapper.getBlogList().forEach(System.out::println);
        System.out.println(blogMapper.getBlogById(13));
    }

}
