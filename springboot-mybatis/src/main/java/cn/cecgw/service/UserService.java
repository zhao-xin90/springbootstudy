package cn.cecgw.service;

import cn.cecgw.entity.Blog;
import cn.cecgw.entity.User;

import java.util.List;

/**
 * @version 1.0
 * @description:UserService
 * @description: TODO
 * @author: 赵新
 * @date: 2021/5/27 15:19
 */
public interface UserService {
    List<User> getUserList();
    User getUserById(Integer id);
    int updateUser(User user);
    int addUser(User user);
    int deleteUserById(Integer id);
    List<User> getUserListByCondition(User user);
}
