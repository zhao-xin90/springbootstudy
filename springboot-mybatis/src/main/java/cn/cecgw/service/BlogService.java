package cn.cecgw.service;

import cn.cecgw.entity.Blog;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @version 1.0
 * @description:BlogService
 * @description: TODO
 * @author: 赵新
 * @date: 2021/5/27 15:19
 */

public interface BlogService {
    List<Blog> getBlogList();
    Blog getBlogById(Integer id);
    int updateBlog(Blog blog);
    int addBlog(Blog blog);
    int deleteBlogById(Integer id);
    List<Blog> getBlogListByCondition(Blog blog);
}
