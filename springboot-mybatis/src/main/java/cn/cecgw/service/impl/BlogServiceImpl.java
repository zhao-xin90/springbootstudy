package cn.cecgw.service.impl;

import cn.cecgw.entity.Blog;
import cn.cecgw.mapper.BlogMapper;
import cn.cecgw.service.BlogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @version 1.0
 * @description:BlogServiceImpl
 * @description: TODO
 * @author: 赵新
 * @date: 2021/5/27 15:19
 */
@Service
public class BlogServiceImpl implements BlogService {
    @Autowired
    BlogMapper blogMapper;
    @Override
    public List<Blog> getBlogList() {
        return blogMapper.getBlogList();
    }

    @Override
    public Blog getBlogById(Integer id) {
        return blogMapper.getBlogById(id);
    }

    @Override
    public int updateBlog(Blog blog) {
        return blogMapper.updateBlog(blog);
    }

    @Override
    public int addBlog(Blog blog) {
        return blogMapper.addBlog(blog);
    }

    @Override
    public int deleteBlogById(Integer id) {
        return blogMapper.deleteBlogById(id);
    }

    @Override
    public List<Blog> getBlogListByCondition(Blog blog) {
        return blogMapper.getBlogListByCondition(blog);
    }
}
