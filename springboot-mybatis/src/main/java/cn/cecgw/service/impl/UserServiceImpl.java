package cn.cecgw.service.impl;

import cn.cecgw.entity.User;
import cn.cecgw.mapper.UserMapper;
import cn.cecgw.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @version 1.0
 * @description:UserServiceImpl
 * @description: TODO
 * @author: 赵新
 * @date: 2021/5/27 15:20
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserMapper userMapper;
    @Override
    public List<User> getUserList() {
        return userMapper.getUserList();
    }

    @Override
    public User getUserById(Integer id) {
        return userMapper.getUserById(id);
    }

    @Override
    public int updateUser(User user) {
        return userMapper.updateUser(user);
    }

    @Override
    public int addUser(User user) {
        return userMapper.addUser(user);
    }

    @Override
    public int deleteUserById(Integer id) {
        return userMapper.deleteUserById(id);
    }

    @Override
    public List<User> getUserListByCondition(User user) {
        return userMapper.getUserListByCondition(user);
    }
}
