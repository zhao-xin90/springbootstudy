package cn.cecgw.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @version 1.0
 * @description:SwaggerConfig
 * @description: TODO
 * @author: 赵新
 * @date: 2021/5/27 16:01
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket docket() {
        return new Docket(DocumentationType.SWAGGER_2).apiInfo(
                new ApiInfoBuilder()
                        .description("这是学习mybatis的接口文档")
                        .title("Mybatis 接口文档").version("v1.0")
                        .build()
        ).select()
                .apis(RequestHandlerSelectors.basePackage("cn.cecgw.controller"))
                .paths(PathSelectors.any())
                .build().groupName("mybatis");
    }
}
