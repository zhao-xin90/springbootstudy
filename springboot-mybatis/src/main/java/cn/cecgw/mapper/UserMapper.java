package cn.cecgw.mapper;

import cn.cecgw.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @version 1.0
 * @description:UserMapper
 * @description: TODO
 * @author: 赵新
 * @date: 2021/5/27 14:29
 */
@Repository
@Mapper
public interface UserMapper {
     List<User> getUserList();
     User getUserById(Integer id);
     int updateUser(User user);
     int addUser(User user);
     int deleteUserById(Integer id);
     List<User> getUserListByCondition(User user);

}
