package cn.cecgw.mapper;

import cn.cecgw.entity.Blog;
import cn.cecgw.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @version 1.0
 * @description:BlogMapper
 * @description: TODO
 * @author: 赵新
 * @date: 2021/5/27 14:30
 */
@Repository
@Mapper
public interface BlogMapper {
    List<Blog> getBlogList();
    Blog getBlogById(Integer id);
    int updateBlog(Blog blog);
    int addBlog(Blog blog);
    int deleteBlogById(Integer id);
    List<Blog> getBlogListByCondition(Blog blog);
}
