package cn.cecgw.controller;

import cn.cecgw.entity.Blog;
import cn.cecgw.entity.User;
import cn.cecgw.service.BlogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @version 1.0
 * @description:BlogController
 * @description: 博客控制器
 * @author: 赵新
 * @date: 2021/5/27 15:28
 */
@Api("博客控制器")
@RestController
@RequestMapping("/blog")
public class BlogController {
    @Autowired
    BlogService blogService;

    @ApiOperation("获取博客列表")
    @GetMapping("/list")
    public List<Blog> getBlogList() {
        return blogService.getBlogList();
    }

    @ApiOperation("根据条件获取博客列表")
    @GetMapping("/clist")
    public List<Blog> getBlogListByConditon(@ApiParam("博客") Blog blog) {
        return blogService.getBlogListByCondition(blog);
    }

    @ApiOperation("根据ID获取单个博客")
    @GetMapping("/get/{id}")
    public Blog getBlogById(@PathVariable("id") @ApiParam("ID") Integer id) {
        return blogService.getBlogById(id);
    }

    @ApiOperation("根据ID删除博客")
    @DeleteMapping("/delete/{id}")
    public int deleteBlog(@PathVariable("id") @ApiParam("ID") Integer id) {
        return blogService.deleteBlogById(id);
    }

    @ApiOperation("更新博客")
    @PutMapping("/update")
    public int updateBlog(@ApiParam("博客") Blog blog) {
        return blogService.updateBlog(blog);
    }

    @ApiOperation("添加博客")
    @PutMapping("/add")
    public int addBlog(@ApiParam("博客") Blog blog) {
        return blogService.addBlog(blog);
    }
}
