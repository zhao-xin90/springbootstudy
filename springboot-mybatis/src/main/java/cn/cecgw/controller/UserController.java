package cn.cecgw.controller;

import cn.cecgw.entity.Blog;
import cn.cecgw.entity.User;
import cn.cecgw.service.BlogService;
import cn.cecgw.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @version 1.0
 * @description:UserController
 * @description: 用户控制器
 * @author: 赵新
 * @date: 2021/5/27 15:27
 */
@Api("用户控制器")
@RestController
public class UserController {
    @Autowired
    UserService userService;

    @ApiOperation("获取用户列表")
    @GetMapping("/list")
    public List<User> getUserList() {
        return userService.getUserList();
    }

    @ApiOperation("根据条件获取用户列表")
    @GetMapping("/clist")
    public List<User> getUserListByConditon(@ApiParam("用户") User user) {
        return userService.getUserListByCondition(user);
    }

    @ApiOperation("根据ID获取单个用户")
    @GetMapping("/get/{id}")
    public User getUserById(@PathVariable("id") @ApiParam("ID") Integer id) {
        return userService.getUserById(id);
    }

    @ApiOperation("根据ID删除用户")
    @DeleteMapping("/delete/{id}")
    public int deleteUser(@PathVariable("id") @ApiParam("ID") Integer id) {
        return userService.deleteUserById(id);
    }

    @ApiOperation("更新用户")
    @PutMapping("/update")
    public int updateUser(@ApiParam("用户") User user) {
        return userService.updateUser(user);
    }

    @ApiOperation("添加用户")
    @PutMapping("/add")
    public int addUser(@ApiParam("用户") User user) {
        return userService.addUser(user);
    }
}
