package cn.cecgw.controller;

import jdk.nashorn.internal.ir.WhileNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0
 * @description:IndexController
 * @description: TODO
 * @author: 赵新
 * @date: 2021/5/25 17:26
 */
@Controller
public class IndexController {
    @Autowired
    DataSource dataSource;
    @Autowired
    JdbcTemplate jdbcTemplate;
    @GetMapping("/users")
    @ResponseBody
    public List getUserList() throws Exception{
        ResultSet resultSet = dataSource.getConnection().prepareStatement("select name password from  user ").executeQuery();
        ArrayList<Map> list = new ArrayList<>();
        while (resultSet.next()){
            HashMap<String, Object> map = new HashMap<>();
            map.put("name",resultSet.getString(1));
            map.put("password",resultSet.getString("password"));
            list.add(map);
        }
        return list;
    }

    @GetMapping("/users2")
    @ResponseBody
    public List getUserList2() throws Exception{
        List<Map<String, Object>> list = jdbcTemplate.queryForList("select name password from  user");

        return list;
    }

}
