package cn.cecgw.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @version 1.0
 * @description:Blog
 * @description: 博客
 * @author: 赵新
 * @date: 2021/5/27 14:28
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("博客")
public class Blog {
    @ApiModelProperty("ID")
    private Integer id;
    @ApiModelProperty("标题")
    private String title;
    @ApiModelProperty("作者")
    private String author;
    @ApiModelProperty("创建时间")
    private Date createTime;
    @ApiModelProperty("访问量")
    private Integer views;
}
