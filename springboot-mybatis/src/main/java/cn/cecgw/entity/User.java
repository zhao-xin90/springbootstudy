package cn.cecgw.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NegativeOrZero;

/**
 * @version 1.0
 * @description:User
 * @description: 用户
 * @author: 赵新
 * @date: 2021/5/27 14:24
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("用户")
public class User {
    @ApiModelProperty("ID")
    private Integer id;
    @ApiModelProperty("用户名")
    private String name;
    @ApiModelProperty("密码")
    private String password;
}
