package cn.cecgw;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@EnableAdminServer
@SpringBootApplication
public class SpringbootActuactorAdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootActuactorAdminApplication.class, args);
    }


}
