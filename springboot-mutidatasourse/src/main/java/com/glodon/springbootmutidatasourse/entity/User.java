package com.glodon.springbootmutidatasourse.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author zhaoxin
 * @version 1.0
 * @description: 用户实力类
 * @date 2021/11/12 9:21
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User implements Serializable {
    private Integer id;
    private String name;
}
