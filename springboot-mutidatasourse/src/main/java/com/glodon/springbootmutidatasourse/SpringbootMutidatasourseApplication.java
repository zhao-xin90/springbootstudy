package com.glodon.springbootmutidatasourse;

import com.glodon.springbootmutidatasourse.controller.UserController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import java.util.logging.Logger;

@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class })
public class SpringbootMutidatasourseApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringbootMutidatasourseApplication.class, args);
    }

}
