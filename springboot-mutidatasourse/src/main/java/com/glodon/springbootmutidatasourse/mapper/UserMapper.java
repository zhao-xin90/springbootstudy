package com.glodon.springbootmutidatasourse.mapper;

import com.glodon.springbootmutidatasourse.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author zhaoxin
 * @version 1.0
 * @description: User的持久化层
 * @date 2021/11/12 9:23
 */
@Mapper
@Repository
public interface UserMapper {
    User getUserById(@Param("id") Integer id);
    List<User> commonList();
}
