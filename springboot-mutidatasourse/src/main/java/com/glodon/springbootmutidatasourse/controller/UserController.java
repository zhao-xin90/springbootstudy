package com.glodon.springbootmutidatasourse.controller;

import com.glodon.springbootmutidatasourse.entity.User;
import com.glodon.springbootmutidatasourse.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.logging.Logger;

/**
 * @author zhaoxin
 * @version 1.0
 * @description: User控制层
 * @date 2021/11/12 10:33
 */
@RestController
public class UserController {
    @Autowired
    public UserService userService;
    @GetMapping("/user/get/{id}")
    public User getUserById(@PathVariable("id")Integer id){
        return userService.getUserById(id);
    }
    @GetMapping("/user/list")
    public List<User> getUseList(){
        System.out.println("2222222");
        return userService.commonList();
    }
}
