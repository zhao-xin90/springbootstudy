package com.glodon.springbootmutidatasourse.datasource;



import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
 * @author zhaoxin
 * @version 1.0
 * @description: 动态数据源 获取数据源（依赖于 spring）  定义一个类继承AbstractRoutingDataSource实现determineCurrentLookupKey方法，该方法可以实现数据库的动态切换
 * @date 2021/11/12 10:03
 */
public class DynamicDataSource extends AbstractRoutingDataSource {
    /**
     * 获取与数据源相关的key
     * 此key是Map<String,DataSource> resolvedDataSources 中与数据源绑定的key值
     * 在通过determineTargetDataSource获取目标数据源时使用
     */
    @Override
    protected Object determineCurrentLookupKey() {
        return DynamicDataSourceContextHolder.getDataSourceType();
    }
}
