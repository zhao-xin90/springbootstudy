package com.glodon.springbootmutidatasourse.utils.constansts;

/**
 * @author zhaoxin
 * @version 1.0
 * @description: 数据源类型
 * @date 2021/11/12 9:45
 */
public enum DataSourceType {
    /**
     * 主库
     */
    MASTER,
    /**
     * 从库
     */
    SLAVE
}
