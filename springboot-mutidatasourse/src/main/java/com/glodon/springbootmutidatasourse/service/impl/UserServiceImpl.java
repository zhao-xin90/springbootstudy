package com.glodon.springbootmutidatasourse.service.impl;

import com.glodon.springbootmutidatasourse.annotation.DataSource;
import com.glodon.springbootmutidatasourse.entity.User;
import com.glodon.springbootmutidatasourse.mapper.UserMapper;
import com.glodon.springbootmutidatasourse.service.UserService;
import com.glodon.springbootmutidatasourse.utils.constansts.DataSourceType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zhaoxin
 * @version 1.0
 * @description: User的Service类
 * @date 2021/11/12 9:29
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    public UserMapper userMapper;
    @Override
    @DataSource(name=DataSourceType.SLAVE)
    public User getUserById(Integer id) {
        return userMapper.getUserById(id);
    }

    @Override
    public List<User> commonList() {
        return userMapper.commonList();
    }
}
