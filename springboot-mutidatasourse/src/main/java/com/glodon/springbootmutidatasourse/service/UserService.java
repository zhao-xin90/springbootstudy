package com.glodon.springbootmutidatasourse.service;

import com.glodon.springbootmutidatasourse.entity.User;

import java.util.List;

/**
 * @author zhaoxin
 * @version 1.0
 * @description: User服务类
 * @date 2021/11/12 9:28
 */
public interface UserService {

    /**
     * @description:  根据Id获取User
     * @param: id
     * @return:  User
     * @author zhaoxin
     * @date: 2021/11/12 9:31
     */
    User getUserById(Integer id);
    
    /** 
     * @description: 获取User集合
     * @return:  User集合
     * @author  zhaoxin
     * @date: 2021/11/12 9:32
     */ 
    List<User> commonList();
}
