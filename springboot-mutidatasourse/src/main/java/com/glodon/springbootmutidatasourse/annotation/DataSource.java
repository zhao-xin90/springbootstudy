package com.glodon.springbootmutidatasourse.annotation;

import com.glodon.springbootmutidatasourse.utils.constansts.DataSourceType;

import java.lang.annotation.*;

/**
 * @author zhaoxin
 * @version 1.0
 * @description: Datasource 注解用来跟距名称切换数据源
 * @date 2021/11/12 9:39
 */
@Target({ElementType.METHOD,ElementType.TYPE})
@Inherited
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface DataSource {
    DataSourceType name() default DataSourceType.MASTER;
}
