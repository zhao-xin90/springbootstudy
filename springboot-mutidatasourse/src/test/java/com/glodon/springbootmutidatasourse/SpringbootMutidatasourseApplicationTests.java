package com.glodon.springbootmutidatasourse;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.InputStream;
import java.util.Random;
import java.util.stream.IntStream;


class SpringbootMutidatasourseApplicationTests {

    @Test
    void contextLoads() {
        ThreadLocal<String> local=new ThreadLocal<>();
        Random random = new Random();
        IntStream.range(1,5).forEach(a->new Thread(()->{
            local.set(a+" "+ random.nextInt(10));
            System.out.println("线程和local值分别是"+local.get());
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start());
    }

}
