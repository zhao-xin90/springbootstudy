package cn.cecgw.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @version 1.0
 * @description:AopController
 * @description: TODO
 * @author: 赵新
 * @date: 2021/6/1 15:48
 */
@RestController
public class AopController {
    @RequestMapping("/first")
    public String first(){
        return "first content";
    }
    @RequestMapping("/doError")
    public int doError(){
        return 6/0;
    }
}
