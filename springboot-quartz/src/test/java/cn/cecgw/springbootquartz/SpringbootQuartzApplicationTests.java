package cn.cecgw.springbootquartz;

import cn.cecgw.springbootquartz.quartz.TestJob;
import cn.cecgw.springbootquartz.service.QuartzManager;
import cn.cecgw.springbootquartz.service.SpringContextUtil;
import cn.cecgw.springbootquartz.service.TestService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
@RunWith(SpringRunner.class)
@SpringBootTest
class SpringbootQuartzApplicationTests {
    @Autowired
    QuartzManager quartzManager;
    @Test
    void test() throws Exception {
        String content="ssss";
        String jobName = "动态任务调度"+content;
        String jobGroupName = "任务组名";
        String triggerName = "触发器名"+content;
        String triggerGroupName = "触发器组名";
        String time = "20/20 * * * * ?";

        quartzManager.addJob(jobName,jobGroupName,triggerName,triggerGroupName, TestJob.class,time);
        HashMap<String, String> map = new HashMap<>();
        map.put("sql","select * from sys_dict");
        quartzManager.addJob("aaa", TestJob.class,"20/20 * * * * ?",map);
        map.put("sql","select * from sys_user");
        quartzManager.addJob("bbb", TestJob.class,"10/20 * * * * ?",map);
    }

    @Test
    void test2(){
        TestService service = SpringContextUtil.getBean(TestService.class);
        service.hello("231313");

    }

}
