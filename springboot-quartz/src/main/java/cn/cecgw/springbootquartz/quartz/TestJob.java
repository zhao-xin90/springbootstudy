package cn.cecgw.springbootquartz.quartz;

import cn.cecgw.springbootquartz.service.TestService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @version 1.0
 * @description:TestJob
 * @description: TODO
 * @author: 赵新
 * @date: 2021/5/10 10:27
 */
public class TestJob implements Job {

   // @Autowired
   // private TestService testService;

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        String jobName = context.getJobDetail().getKey().toString().substring(8);
        System.out.println(jobName+"定时任务开启..");
        //testService.hello(jobName);
        System.out.println(jobName+"定时任务结束..");
    }

}
