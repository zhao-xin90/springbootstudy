package cn.cecgw.springbootquartz.service;

import org.springframework.stereotype.Component;

/**
 * @version 1.0
 * @description:TestService
 * @description: TODO
 * @author: 赵新
 * @date: 2021/5/10 10:28
 */
@Component
public class TestService {
    public void hello(String jobName){
        System.out.println("Hello"+jobName);
    }
}
