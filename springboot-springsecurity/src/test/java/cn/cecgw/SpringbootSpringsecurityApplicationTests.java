package cn.cecgw;

import org.bouncycastle.crypto.params.Argon2Parameters;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;
import org.springframework.security.crypto.scrypt.SCryptPasswordEncoder;

import static org.junit.Assert.assertTrue;

@SpringBootTest
class SpringbootSpringsecurityApplicationTests {

    @Test
    void contextLoads() {
        UserDetails build = User.withDefaultPasswordEncoder()
                .username("user")
                .password("password")
                .roles("user","vip")
                .build();
        System.out.println(build.getPassword());
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String result = encoder.encode("myPassword");
        System.out.println(result);
        assertTrue(encoder.matches("myPassword", result));
        Argon2PasswordEncoder encoder2 = new Argon2PasswordEncoder();
        String result2 = encoder2.encode("myPassword");
        System.out.println(result2);
        assertTrue(encoder.matches("myPassword", result));

    }
    @Test
    void contextLoads1() {
        Pbkdf2PasswordEncoder encoder = new Pbkdf2PasswordEncoder();
        String result = encoder.encode("myPassword");
        System.out.println(result);
        assertTrue(encoder.matches("myPassword", result));
        // Create an encoder with all the defaults
        SCryptPasswordEncoder encoder2 = new SCryptPasswordEncoder();
        String result2 = encoder2.encode("myPassword");
        System.out.println(result2);
        assertTrue(encoder2.matches("myPassword", result2));
    }

}
