package cn.cecgw.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @version 1.0
 * @description:SwaggerConfig
 * @description: TODO
 * @author: 赵新
 * @date: 2021/5/31 9:51
 */
@EnableSwagger2
@Configuration
public class SwaggerConfig {
    @Bean
    public Docket docket() {
      return   new Docket(DocumentationType.SWAGGER_2)
                .groupName("zhaoxin")
                .apiInfo(new ApiInfoBuilder()
                        .version("v1.0")
                        .title("springSecurity")
                        .description("This is a springSecurity Demo")
                        .build())
                .select()
                .apis(RequestHandlerSelectors.basePackage("cn.cecgw.controller"))
                .paths(PathSelectors.any()).build();
    }
}
