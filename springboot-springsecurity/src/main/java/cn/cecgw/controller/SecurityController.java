package cn.cecgw.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @version 1.0
 * @description:SecurityController
 * @description: TODO
 * @author: 赵新
 * @date: 2021/5/31 9:16
 */
@Controller
public class SecurityController {
    @GetMapping({"/index","/"})
    public String index(){
        return "index";
    }
    @GetMapping("/toLogin")
    public String login(){
        return "views/login";
    }
    @GetMapping("/level1/{id}")
    public String level1(@PathVariable("id") Integer id){
        return "views/level1/"+id;
    }
    @GetMapping("/level2/{id}")
    public String level2(@PathVariable("id") Integer id){
        return "views/level2/"+id;
    }
    @GetMapping("/level3/{id}")
    public String level3(@PathVariable("id") Integer id){
        return "views/level3/"+id;
    }
    @GetMapping({"/login"})
    public String login1(){
        System.out.println("111111111111");
        return "index";
    }
}
