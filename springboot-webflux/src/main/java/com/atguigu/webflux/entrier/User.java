package com.atguigu.webflux.entrier;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author zhaoxin
 * @version 1.0.0
 * @ClassName User.java
 * @Description User实体类
 * @createTime 2021年12月31日 09:43:00
 */
@Data
@NoArgsConstructor
@ToString
@AllArgsConstructor
public class User  implements Serializable {
    private String name;
    private String gender;
    private Integer age;
}
