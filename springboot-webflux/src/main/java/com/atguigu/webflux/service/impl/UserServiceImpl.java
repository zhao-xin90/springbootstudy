package com.atguigu.webflux.service.impl;

import com.atguigu.webflux.entrier.User;
import com.atguigu.webflux.service.UserService;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.Map;

/**
 * @author zhaoxin
 * @version 1.0.0
 * @ClassName UserServiceImpl.java
 * @Description UserServiceImpl
 * @createTime 2021年12月31日 09:48:00
 */
@Service
public class UserServiceImpl implements UserService {
    private final Map<Integer,User> map=new HashMap<Integer, User>();

    public UserServiceImpl() {
        map.put(1,new User("赵新","男",31));
        map.put(2,new User("李盼盼","女",31));
        map.put(3,new User("赵依依","女",2));

    }
    //查询多个用户
    @Override
    public Mono<User> getUserById(int id) {
        return Mono.justOrEmpty(map.get(id));
    }
    //查询多个用户
    @Override
    public Flux<User> getAllUser() {
        return Flux.fromIterable(map.values());
    }
    //添加用户
    @Override
    public Mono<Void> saveUserInfo(Mono<User> user) {

        return user.doOnNext(user1 -> map.put(map.size()+1,user1 )).thenEmpty(Mono.empty());
    }
}
