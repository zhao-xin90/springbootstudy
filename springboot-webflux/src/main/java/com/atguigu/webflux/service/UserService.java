package com.atguigu.webflux.service;

import com.atguigu.webflux.entrier.User;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * @author zhaoxin
 * @version 1.0.0
 * @ClassName UserService.java
 * @Description 用户操作接口
 * @createTime 2021年12月31日 09:45:00
 */
public interface UserService {
    //根据 id 查询用户
    Mono<User> getUserById(int id);
    //查询所有用户
    Flux<User> getAllUser();
    //添加用户
    Mono<Void> saveUserInfo(Mono<User> user);
}
