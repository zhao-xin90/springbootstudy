package com.atguigu.webflux.learn;

import reactor.core.publisher.Mono;

/**
 * @author zhaoxin
 * @version 1.0.0
 * @ClassName ObserverDemo1.java
 * @Description 测试 访问权限符
 * @createTime 2021年12月30日 17:36:00
 */
public class ObserverDemo1 {
    public static void main(String[] args) {
        ObserverDemo observerDemo=new ObserverDemo();
        observerDemo.test();
        Mono.fromRunnable(() -> {
            System.out.println("thread run");
            throw new RuntimeException("thread run error");
        }).subscribe(System.out::println, System.err::println);

        Mono.fromSupplier(() -> "create from supplier").subscribe(System.out::println);
    }
}
