package com.atguigu.webflux.learn;

import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * @author zhaoxin
 * @version 1.0.0
 * @ClassName ReactorDemo.java
 * @Description 学习Reactor
 * @createTime 2021年12月30日 17:48:00
 */
public class ReactorDemo {
    public static void main(String[] args) {
        Disposable subscribe = Mono.just(1).subscribe();
     Flux.just(1, 2, 3, 4).subscribe(System.out::println);

    }
}
