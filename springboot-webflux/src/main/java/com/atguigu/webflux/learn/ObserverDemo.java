package com.atguigu.webflux.learn;

import org.junit.jupiter.api.Test;

import java.util.Observable;

/**
 * @author zhaoxin
 * @version 1.0.0
 * @ClassName ObserverDemo.java
 * @Description 学习观察者模式
 * @createTime 2021年12月30日 17:29:00
 */
public class ObserverDemo extends Observable {
    protected void test(){
        System.out.println("测试Protected");
    }
    private void  test1(){
        System.out.println("ce");
    }
    @Test
    public void  test2() {
        ObserverDemo observable = new ObserverDemo();
        observable.addObserver((o,arg)->{
            System.out.println("发生变化1");
        });
        observable.addObserver((o,arg)->{
            System.out.println("发生变化2");
        });
        observable.setChanged();
        observable.notifyObservers();
        observable.test1();
    }
}
