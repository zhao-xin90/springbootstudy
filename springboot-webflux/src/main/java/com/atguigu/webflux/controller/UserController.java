package com.atguigu.webflux.controller;

import com.atguigu.webflux.entrier.User;
import com.atguigu.webflux.service.UserService;
import javafx.scene.chart.PieChart;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;

/**
 * @author zhaoxin
 * @version 1.0.0
 * @ClassName UserController.java
 * @Description TODO
 * @createTime 2021年12月31日 09:59:00
 */
@RestController
public class UserController {
    // 注入 service
    @Autowired
    private UserService userService;

    // id 查询
    @GetMapping("/user/{id}")
    public Mono<User> geetUserId(@PathVariable int id) {
        return userService.getUserById(id);
    }

    //查询所有
    @GetMapping("/user")
    public Flux<User> getUsers() {
        return userService.getAllUser();
    }

    //添加
    @PostMapping(value = "/saveuser")
    public Mono<Void> saveUser(@RequestBody Mono<User> user) {

        Mono<User> www = user.switchIfEmpty(Mono.just(new User("www", "333", 53))).retry();
        System.out.println(www);
        user.subscribe(user1 -> {
            System.out.println(user1);
        });
        System.out.println(user);
     /*   System.out.println("moqq="+userMono1.blockOptional().get());
        Mono<User> userMono = Mono.just(user);*/
        return userService.saveUserInfo(user);
    }
}