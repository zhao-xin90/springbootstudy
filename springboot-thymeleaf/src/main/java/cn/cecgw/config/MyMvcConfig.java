package cn.cecgw.config;

import cn.cecgw.component.MyHandlerInterceptor;
import cn.cecgw.component.MyLocaleResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


/**
 * @version 1.0
 * @description:MyMvcConfig
 * @description: TODO
 * @author: 赵新
 * @date: 2021/5/21 17:06
 */
@Configuration
public class MyMvcConfig implements WebMvcConfigurer {

    @Bean
    public LocaleResolver localeResolver() {
        return new MyLocaleResolver();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new MyHandlerInterceptor())
                .excludePathPatterns("/","/druid","/druid/**","/index","/login"
                        ,"/js/**","/css/**","/font/**","/images/**","/font-awesome/**","/actuator/**");
    }
}

