package cn.cecgw.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @version 1.0
 * @description:IndexController
 * @description: TODO
 * @author: 赵新
 * @date: 2021/5/21 16:18
 */
@Controller
public class IndexController {
    @Autowired
    DataSource dataSource;
    @RequestMapping("/index")
    public String index(){
        System.out.println("lllll");
        return "index";
    }
    @RequestMapping("/login")
    public String login(String username, String password,HttpSession session, Model model)throws SQLException{
        Object map = model.asMap();
        System.out.println(map);
        System.out.println(model.getAttribute("map"));
        System.out.println(username+"----"+password);

        PreparedStatement preparedStatement = dataSource.getConnection().prepareStatement("select `name`,`password` from `user` where `name`=  '"+username +"'");
        ResultSet resultSet = preparedStatement.executeQuery();
       String DPassword=null;
        while(resultSet.next()){
            DPassword=resultSet.getString("password");
        }
        if(DPassword!=null&&DPassword.equals(password)){
            session.setAttribute("username",username);
            model.addAttribute("menuFlag","main");
            return "main";
        }else {
            model.addAttribute("msg","用户名或者密码不对");
            return "index";
        }



    }

    @RequestMapping("/loginOut")
    public String login(HttpSession session){
        System.out.println(session.getAttribute("username"));
        session.invalidate();

        return "redirect:index";

    }
    @RequestMapping("/main")
    public String login(Model model){
        model.addAttribute("menuFlag","main");
        return "main";
    }
    @RequestMapping("/ui")
    public String uiElements(Model model){
        System.out.println("ui");
        model.addAttribute("menuFlag","ui");
        return "ui-elements";
    }
    @RequestMapping("/tabPanel")
    public String tabPanel(Model model){
        System.out.println("tabPanel");
        model.addAttribute("menuFlag","tabPanel");
        return "tab-panel";
    }

    @RequestMapping("/chart")
    public String chart(Model model){
        System.out.println("chart");
        model.addAttribute("menuFlag","chart");
        return "chart";
    }

    @RequestMapping("/table")
    public String table(Model model){
        System.out.println("table");
        model.addAttribute("menuFlag","table");
        return "table";
    }

    @RequestMapping("/form")
    public String form(Model model){
        System.out.println("form");
        model.addAttribute("menuFlag","form");
        return "form";
    }

    @RequestMapping("/empty1")
    public String empty(Model model){
        System.out.println("empty");
        model.addAttribute("menuFlag","empty1");
        return "empty";
    }


}
