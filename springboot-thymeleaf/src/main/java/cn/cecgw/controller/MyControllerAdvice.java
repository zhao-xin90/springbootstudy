package cn.cecgw.controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

/**
 * @version 1.0
 * @description:MyControllerAdvice
 * @description: TODO
 * @author: 赵新
 * @date: 2021/6/1 10:57
 */
@ControllerAdvice
public class MyControllerAdvice {
    @ModelAttribute
    public Map<String,Object> map(Model model){
       Map<String, Object> map = new HashMap<>(3);
        map.put("name","赵新");
        map.put("age",31);
        return map;
    }
    @ExceptionHandler(Exception.class)
    public ModelAndView cutomException(Exception e){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("message", e.getMessage());
        modelAndView.setViewName("myerror");
        return modelAndView;

    }
}
