package cn.cecgw.actuactor;

import org.springframework.boot.actuate.info.Info;
import org.springframework.boot.actuate.info.InfoContributor;
import org.springframework.stereotype.Component;

/**
 * @author 赵新
 * @version 1.0
 * @description:MyInfoContributor TODO
 * @date 2021/6/10 16:37
 */
@Component
public class MyInfoContributor implements InfoContributor {
    @Override
    public void contribute(Info.Builder builder) {
        builder.withDetail("version","1.0.0").withDetail("javaVersion","108").build();

    }
}
