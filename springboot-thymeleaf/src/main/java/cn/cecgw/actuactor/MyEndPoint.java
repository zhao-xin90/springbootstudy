package cn.cecgw.actuactor;

import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.boot.actuate.endpoint.annotation.WriteOperation;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 赵新
 * @version 1.0
 * @description:MyEndPoint TODO
 * @date 2021/6/10 16:46
 */
@Component
@Endpoint(id="thye")
public class MyEndPoint {
    @ReadOperation
    public Map<String,String> read(){
        HashMap<String, String> map = new HashMap<>();
        map.put("userName","赵新");
        map.put("password","123456");
        return map;
    }
    @WriteOperation
    public void write(){
        System.out.println("自定义endpoint写");
    }
}
