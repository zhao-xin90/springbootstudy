package cn.cecgw.actuactor;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Locale;

/**
 * @author 赵新
 * @version 1.0
 * @description:MyhealthIndicator TODO
 * @date 2021/6/10 16:23
 */
@Component
public class MyhealthIndicator implements HealthIndicator {
    @Override
    public Health health() {
        Health.Builder builder = new Health.Builder();
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("msg","应用很健康");

        hashMap.put("time", LocalDateTime.now().toString());
        builder.up().withDetail("code","健康").withDetails(hashMap);
        return builder.build();
    }
}
