package cn.cecgw.component;

import org.springframework.util.StringUtils;
import org.springframework.web.servlet.LocaleResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;

/**
 * @version 1.0
 * @description:MyLocaleResolver
 * @description: TODO
 * @author: 赵新
 * @date: 2021/5/24 9:54
 */
public class MyLocaleResolver implements LocaleResolver {
    @Override
    public Locale resolveLocale(HttpServletRequest request) {
        Locale locale=null;
       String l= request.getParameter("l");
       if(!StringUtils.isEmpty(l)){
           String[] s = l.split("_");
           locale=new Locale(s[0],s[1]);
           request.getSession().setAttribute("locale",locale);
       }else {
           if (request.getSession().getAttribute("locale")==null){
               locale=Locale.getDefault();
           }else{
               locale=(Locale) request.getSession().getAttribute("locale");
           }

       }
      // System.out.println("locale="+locale);

        return locale;
    }

    @Override
    public void setLocale(HttpServletRequest request, HttpServletResponse response, Locale locale) {
        System.out.println("请求---"+request.getLocale());
    }
}
