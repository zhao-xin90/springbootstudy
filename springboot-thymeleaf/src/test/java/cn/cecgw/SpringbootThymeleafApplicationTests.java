package cn.cecgw;

import com.alibaba.druid.pool.DruidDataSource;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.sql.DataSource;
import java.sql.ResultSet;



@SpringBootTest
class SpringbootThymeleafApplicationTests {
    @Autowired
    DataSource dataSource;
    @Test
    void contextLoads() throws Exception {
        System.out.println(dataSource.getClass());
        System.out.println(dataSource.getConnection().getClass());
        DruidDataSource druidDataSource = (DruidDataSource) dataSource;
        System.out.println("druidDataSource 数据源最大连接数：" + druidDataSource.getMaxActive());
        System.out.println("druidDataSource 数据源初始化连接数：" + druidDataSource.getInitialSize());


        ResultSet resultSet = dataSource.getConnection().createStatement().executeQuery("select * from user");
        while (resultSet.next()){
            System.out.println(resultSet.getString("id"));
            System.out.println(resultSet.getString("name"));
            System.out.println(resultSet.getString("password"));
        }
    }

}
