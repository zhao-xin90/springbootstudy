package com.glodon.fastdfs.config;

import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author zhaoxin
 * @version 1.0
 * @description: Mybatis配置文件
 * @date 2021/11/16 13:35
 */
@Configuration
@MapperScan(basePackages = "com.glodon.fastdfs.mapper")
public class MybatisConfig {
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor(){
        return new MybatisPlusInterceptor();
    }
}
