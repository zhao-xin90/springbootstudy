package com.glodon.fastdfs.service.impl;

import com.glodon.fastdfs.mapper.CreditorInfoMapper;
import com.glodon.fastdfs.model.CreditorInfo;
import com.glodon.fastdfs.service.CreditorInfoService;

import com.glodon.fastdfs.utils.FastDFSUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhaoxin
 * @version 1.0
 * @description: TODO
 * @date 2021/11/16 11:13
 */
@Service
public class CreditorInfoServiceImpl implements CreditorInfoService {
    @Resource
    public CreditorInfoMapper creditoInfoMapper;

    @Override
    public List<CreditorInfo> getAllCredentialInfo() {
        return creditoInfoMapper.selectAll();
    }

    @Override
    public CreditorInfo selectById(Integer id) {
        return creditoInfoMapper.selectByPrimaryKey(id);
    }

    @Override
    public void updateFileInfo(CreditorInfo creditorInfo) {
        creditoInfoMapper.updateByPrimaryKey(creditorInfo);
    }

    @Override
    public void deleteFileById(Integer id) {
        CreditorInfo creditorInfo=creditoInfoMapper.selectByPrimaryKey(id);
        FastDFSUtil.delete(creditorInfo.getGroupName(),creditorInfo.getRemoteFilePath());
        creditorInfo.setRemoteFilePath("");
        creditorInfo.setGroupName("");
        creditoInfoMapper.updateByPrimaryKeySelective(creditorInfo);
    }
}
