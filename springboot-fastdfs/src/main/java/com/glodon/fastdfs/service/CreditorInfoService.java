package com.glodon.fastdfs.service;

import com.glodon.fastdfs.mapper.CreditorInfoMapper;
import com.glodon.fastdfs.model.CreditorInfo;


import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhaoxin
 * @version 1.0
 * @description: TODO
 * @date 2021/11/16 11:13
 */
public interface CreditorInfoService {
  List<CreditorInfo> getAllCredentialInfo();
  CreditorInfo selectById(Integer id);

  void updateFileInfo(CreditorInfo creditorInfo);

  void deleteFileById(Integer id);
}
