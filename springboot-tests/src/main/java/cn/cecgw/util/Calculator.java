package cn.cecgw.util;

/**
 * @author 赵新
 * @version 1.0
 * @description:Calculator 二则运算
 * @date 2021/6/9 15:34
 */
public class Calculator {
    public int add(int a, int b) {
        return a + b;
    }

    public int multiply(int a, int b) {
        return a * b;
    }

    public int divide(int a, int b) {
        return a / b;
    }

    public int minus(int a, int b) {
        return a - b;
    }
}
