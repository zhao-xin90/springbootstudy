package cn.cecgw;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.condition.EnabledOnOs;
import org.junit.jupiter.api.condition.OS;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

/**
 * @author 赵新
 * @version 1.0
 * @description:StandardTest TODO
 * @date 2021/6/9 14:57
 */
@DisplayName("标准测试")
public class StandardTest {
    @DisplayName("测试开始")
    @BeforeAll
   static void beforeAll(){
        System.out.println("测试开始");
    }

    @DisplayName("前置条件")
    @BeforeEach
    void beforeTest(){
        System.out.println("每次测试之前执行");
    }
    @DisplayName("测试方法一")
    @Test
    void test1(){
        System.out.println("正式测试1");
    }
    @DisplayName("测试方法二")
    @EnabledOnOs({OS.LINUX})
    @Test
    void test2(){
        System.out.println("正式测试2");
    }
    @Disabled("禁用测试")
    @Test

    @DisplayName("正式测试3")
    void test3(){
        System.out.println("正式测试3");
    }
    @DisplayName("重复测试")
    @RepeatedTest(5)
    void test4(){
        System.out.println("重复测试");
    }
    @DisplayName("参数测试")
    @ParameterizedTest
    @ValueSource(ints = {1,2,3,4,5})
    void test5(int i){
        System.out.println("参数测试"+i);
    }
    @DisplayName("快速失败")
    @Test
    void test6(){
        Assumptions.assumeTrue("abc".contains("Z"));
        Assertions.fail("快速失败11");
    }
    @DisplayName("后置处理")
    @AfterEach
    void afterTest(){
        System.out.println("每次执行之后执行");
    }
    @DisplayName("测试结束")
   static @AfterAll
     void afterAll(){
        System.out.printf("测试结束");
    }
}
