package cn.cecgw;

import cn.cecgw.util.Calculator;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assumptions.*;

/**
 * @author 赵新
 * @version 1.0
 * @description:AssumptionsDemo TODO
 * @date 2021/6/9 15:57
 */
public class AssumptionsDemo {
    private final Calculator calculator = new Calculator();

    @Test
    void testOnlyOnCiServer() {
        assumeTrue("CI".equals(System.getenv("ENV")));
        // remainder of test
    }

    @Test
    void testOnlyOnDeveloperWorkstation() {
        assumeTrue("DEV".equals(System.getenv("ENV")),
                () -> "Aborting test: not on developer workstation");
        // remainder of test
    }

    @Test
    void testInAllEnvironments() {
        assumingThat("CI".equals(System.getenv("ENV")),
                () -> {
                    System.out.println("11111");
                    // perform these assertions only on the CI server
                    assertEquals(2, calculator.divide(4, 2));
                });
        System.out.println("222222");
        // perform these assertions in all environments
        assertEquals(42, calculator.multiply(6, 7));
    }
}
