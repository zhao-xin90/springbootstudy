package cn.cecgw.mapper;

import cn.cecgw.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @version 1.0
 * @description:UserMapper
 * @description: TODO
 * @author: 赵新
 * @date: 2021/6/1 13:35
 */
@Mapper
@Repository
public interface UserMapper {
    List<User> queryUserList();
    User queryUserById(Integer id);
    int addUser(User user);
    int updateUser(User user);
    int deleteUser(Integer id);
    User queryUserByName(String name);
}
