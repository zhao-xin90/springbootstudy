package cn.cecgw.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @version 1.0
 * @description:User
 * @description: TODO
 * @author: 赵新
 * @date: 2021/6/1 13:32
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
    private Integer id;
    private String name;
    private String password;
    private String perms;
}
