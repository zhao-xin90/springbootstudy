package cn.cecgw.service;

import cn.cecgw.entity.User;

import java.util.List;

/**
 * @version 1.0
 * @description:UserService
 * @description: TODO
 * @author: 赵新
 * @date: 2021/6/1 13:45
 */
public interface UserService {
    List<User> queryUserList();
    User queryUserById(Integer id);
    int addUser(User user);
    int updateUser(User user);
    int deleteUser(Integer id);
    User queryUserByName(String name);
}
