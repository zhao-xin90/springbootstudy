package cn.cecgw;

import cn.cecgw.entity.Dog;
import cn.cecgw.entity.Person;
import cn.cecgw.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

@SpringBootTest
class SpringbootHelloApplicationTests {
   // @Autowired
   // Dog dog1;
    @Autowired
    Person person;
    @Autowired
    User user;
    @Test
    void contextLoads() {
      //  System.out.println(dog1);
       System.out.println(person);
        System.out.println(user);
    }

    @Test
    public void test1(){
        List<String> list = Arrays.asList("1", "2", "3", "4", "5");
        List<String> list1 = list.subList(0, 3);
        List<String> list2 = list.subList(3, 5);
        System.out.println(list1);
        System.out.println(list2);
    }
    @Test
    public void test2(){
        System.out.println("赵新");
    }
}
