package cn.cecgw.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @version 1.0
 * @description:HelloController
 * @description: TODO
 * @author: 赵新
 * @date: 2021/5/19 13:49
 */
@RestController
public class HelloController {

    @GetMapping({"/hello"})
    public String get(){
        return "Hello World";
    }

    @PutMapping("/hello")
    public String put(){
        return "put-Hello World";
    }

    @DeleteMapping("/hello")
    public String delete(){
        return "delete-Hello World";
    }
    @PostMapping("/hello")
    public String post(){
        return "post-Hello World";
    }

}
