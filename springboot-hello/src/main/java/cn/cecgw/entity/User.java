package cn.cecgw.entity;

import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * @version 1.0
 * @description:User
 * @description: TODO
 * @author: 赵新
 * @date: 2021/5/19 16:58
 */
@Component
@PropertySource("classpath:user.properties")
@ToString
public class User {
    @Value("${user.aname}")
    private String name;
    @Value("${user.age}")
    private Integer age;
    @Value("${user.sex}")
    private String sex;
}
