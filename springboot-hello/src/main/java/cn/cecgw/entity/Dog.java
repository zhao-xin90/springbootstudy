package cn.cecgw.entity;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @version 1.0
 * @description:Dog
 * @description: TODO
 * @author: 赵新
 * @date: 2021/5/19 16:12
 */
@Component
@Data
public class Dog {
    public Dog() {
        System.out.println("dog111");
    }

    @Value("大黄")
    private String name;
    @Value("15")
    private int age;

    @Override
    public String toString() {
        return "Dog{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
