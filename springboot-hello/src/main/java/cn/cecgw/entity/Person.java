package cn.cecgw.entity;

import lombok.Data;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.*;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @version 1.0
 * @description:Person
 * @description: TODO
 * @author: 赵新
 * @date: 2021/5/19 16:24
 */
@Component
@ConfigurationProperties(prefix = "person")
@Data
@Validated
public class Person {
   // @Email
    @Length(min = 1,max = 100,message = "姓名长度在1到100之间")
    private String name;
    private String last_name;
    @Max(value = 120,message = "年龄不能超过120岁")
    private Integer age;
    @AssertTrue(message = "结果为真")
    //@AssertFalse(message = "结果未假")
    private Boolean happy;
    //@Future
    @Past
    private Date birth;
    private Map<String,Object> maps;
    private Map<String,Object> maps2;
    @Size(min = 1,max = 5,message = "集合的长度在4到5之间")
    private List<Object> lists;
    @NotNull(message = "此属性不为空")
    private List<Object> lists2;
    private Dog dog;
    private Dog dog2;
   @Null(message = "此属性为空")
    private Set set;

   @NotBlank(message = "此属性为空或者null")
   @Email(message = "此属性不是email")
    private String email;

}
