package cn.cecgw.init;

import cn.cecgw.entity.Dog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @version 1.0
 * @description:TestCommandLineRunner2
 * @description: TODO
 * @author: 赵新
 * @date: 2021/5/20 9:29
 */
@Component
@Order(2)
public class TestCommandLineRunner2 implements CommandLineRunner {
    @Autowired
    Dog dog;
    @Override
    public void run(String... args) throws Exception {
        System.out.println("args=2=="+args.length);
        System.out.println("启动加载参数二");
        System.out.println(dog);
    }
}
