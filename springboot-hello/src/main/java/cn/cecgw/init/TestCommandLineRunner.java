package cn.cecgw.init;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @version 1.0
 * @description:TestCommodLineRunner
 * @description: TODO
 * @author: 赵新
 * @date: 2021/5/20 9:27
 */
@Component

public class TestCommandLineRunner implements CommandLineRunner {
    @Value("${person.name}")
    private String name;
    @Override
    public void run(String... args) throws Exception {
        System.out.println("name"+name);
        System.out.println("args=1=="+args.length);
        System.out.println("1111111");
    }
}
