package cn.cecgw.init;

import cn.cecgw.entity.Dog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @version 1.0
 * @description:TestPostConstruct
 * @description: TODO
 * @author: 赵新
 * @date: 2021/5/20 9:37
 */
@Component
public class TestPostConstruct {
    public TestPostConstruct(){
        System.out.println("TestPostConstruct 构造方法");
    }
    @Autowired
    Dog dog;
    @PostConstruct
    public void postConstruct(){
        System.out.println("TestPostConstruct dog="+dog);
    }
}
